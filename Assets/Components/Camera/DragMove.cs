﻿using UnityEngine;
using Assets.Components.Service;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Components {

    public class DragMove : MonoBehaviour {

        public int amountOfForceRecordings = 5;
        public Rect boundary;

        private Vector3? lastPositionMoved = null;
        private Vector3? startPosition;
        private MomentumMover mover = null;

        private Stack<Vector2> recordedForces;

        void Start() {
            Services.GetService<InputService>().registerMouseHeldListener(new InputAction { Action = Held });
            Services.GetService<InputService>().registerMousePressedListener(new InputAction { Action = Pressed });
            Services.GetService<InputService>().registerInputUpListener(new InputAction { Action = Released });

            recordedForces = new Stack<Vector2>(amountOfForceRecordings);

            mover = GetComponent<MomentumMover>();
        }

        private void Update()
        {
            CheckBounds();
        }

        private bool CheckBounds()
        {
            bool wasWithinBounds = true;
            if (transform.position.x > boundary.xMax)
            {
                mover.SetForce(Vector2.zero);
                transform.position = new Vector3(boundary.xMax - 0.001f, transform.position.y, transform.position.z);
                wasWithinBounds = false;
            }
            else if (transform.position.x < boundary.xMin)
            {
                mover.SetForce(Vector2.zero);
                transform.position = new Vector3(boundary.xMin + 0.001f, transform.position.y, transform.position.z);
                wasWithinBounds = false;
            }

            if (transform.position.y > boundary.yMax)
            {
                mover.SetForce(Vector2.zero);
                transform.position = new Vector3(transform.position.x, boundary.yMax - 0.001f, transform.position.z);
                wasWithinBounds = false;
            }
            else if (transform.position.y < boundary.yMin)
            {
                mover.SetForce(Vector2.zero);
                transform.position = new Vector3(transform.position.x, boundary.yMin + 0.001f, transform.position.z);
                wasWithinBounds = false;
            }
            return wasWithinBounds;
        }

        private Vector2 CalculateAverageVector(IEnumerable<Vector2> vectors)
        {
            if (vectors.Count() == 0) return Vector2.zero;
            float x = vectors.Average(a => a.x);
            float y = vectors.Average(a => a.y);
            return new Vector2(x, y);
        }

        void Released(object screenMousePosition)
        {
            if (lastPositionMoved.HasValue == false) return;
            var lastForce = CalculateAverageVector(recordedForces);
            mover.SetForce(new Vector2(lastForce.x, lastForce.y) / Time.deltaTime);
            recordedForces.Clear();
        }

        void Pressed(object screenMousePosition)
        {
            if (!CheckBounds()) return;
            recordedForces.Clear();
            mover.SetForce(Vector2.zero);
            startPosition = Camera.main.ScreenToWorldPoint((Vector3)screenMousePosition);
            lastPositionMoved = null;
        }

        void Held(object screenMousePosition)
        {
            if (startPosition == null)
            {
                return;
            }
            var worldMouse = Camera.main.ScreenToWorldPoint((Vector3)screenMousePosition);
            var diff = startPosition - worldMouse;
            transform.Translate(diff.Value);

            if (recordedForces.Count == amountOfForceRecordings) recordedForces.Pop();
            if(lastPositionMoved.HasValue)
                recordedForces.Push(new Vector2(lastPositionMoved.Value.x, lastPositionMoved.Value.y) - new Vector2(worldMouse.x, worldMouse.y));
            lastPositionMoved = worldMouse;

            if (!CheckBounds())
            {
                startPosition = null;
                return;
            }
        }
    }
}
