﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class MomentumMover : MonoBehaviour
{
    public float xMovement;
    public float yMovement;
    public float maxSpeed;
    public float drag;

    void Update()
    {
        transform.position = new Vector3(transform.position.x + (xMovement * Time.deltaTime), transform.position.y + (yMovement * Time.deltaTime), transform.position.z);
        ApplyDrag();
    }

    public void SetForce(Vector2 force)
    {
        xMovement = 0;
        yMovement = 0;
        AddForce(force);
    }

    public void AddForce(Vector2 force)
    {
        xMovement = xMovement + force.x;
        if (xMovement > 0)
        {
            if (xMovement > maxSpeed)
            {
                xMovement = maxSpeed;
            }
        }
        else if (xMovement < 0)
        {
            if (xMovement < maxSpeed * -1)
            {
                xMovement = -maxSpeed;
            }
        }

        yMovement = yMovement + force.y;
        if (yMovement > 0)
        {
            if (yMovement > maxSpeed)
            {
                yMovement = maxSpeed;
            }
        }
        else if (yMovement < 0)
        {
            if (yMovement < maxSpeed * -1)
            {
                yMovement = -maxSpeed;
            }
        }
    }

    private void ApplyDrag()
    {
        if (xMovement > 0)
        {
            xMovement = xMovement - (drag * Time.deltaTime);
            if (xMovement < 0)
            {
                xMovement = 0;
            }
        }
        else if (xMovement < 0)
        {
            xMovement = xMovement + (drag * Time.deltaTime);
            if (xMovement > 0)
            {
                xMovement = 0;
            }
        }

        if (yMovement > 0)
        {
            yMovement = yMovement - (drag * Time.deltaTime);
            if (yMovement < 0)
            {
                yMovement = 0;
            }
        }
        else if (yMovement < 0)
        {
            yMovement = yMovement + (drag * Time.deltaTime);
            if (yMovement > 0)
            {
                yMovement = 0;
            }
        }
    }
}
