﻿using Assets.Components.Service;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WorldButton : MonoBehaviour {

    // Use this for initialization
    void Start() {
        Services.GetService<InputService>().registerMouseRayCastListener(
            new InputAction { Object = gameObject, Action = Pressed });
    }

    void Pressed(object arg)
    {
        GetComponent<Button>().onClick.Invoke();
    }
}
