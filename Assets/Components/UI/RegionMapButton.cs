﻿using Assets.Components.Service;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RegionMapButton : MonoBehaviour {

    public RegionScriptableObject Region;
    public Color RegionColor;

	void Start () {

        GetComponentInChildren<TextMeshPro>().text = Region.RegionName;
        GetComponentInChildren<TextMeshPro>().color = RegionColor;
        GetComponentInChildren<Button>().onClick.AddListener(delegate { Services.GetService<MapService>().EnterRegion(Region); });
        GetComponentInChildren<SpriteRenderer>().color = RegionColor;
	}
	
}
