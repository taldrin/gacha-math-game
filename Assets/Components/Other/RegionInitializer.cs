﻿using Assets.Components.Service;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionInitializer : MonoBehaviour {

	void Start () {
        var backgroundObj = Services.GetService<RegionService>().GetBackgroundSprite();
        var newObject = Instantiate(backgroundObj);
        newObject.transform.position = Vector3.zero;
	}
}
