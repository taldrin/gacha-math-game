﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Region", menuName="Gacha/Region", order =1)]
public class RegionScriptableObject : ScriptableObject {
    public string RegionName;
    public List<EnemyScriptableObject> AvailableEnemies;
    public GameObject BackgroundSprite;
}
