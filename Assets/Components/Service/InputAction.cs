﻿using System;
using UnityEngine;

namespace Assets.Components.Service
{
    public class InputAction
    {
        public Action<object> Action { get; set; }
        public KeyCode Key { get; set; }
        public object Return { get; set; }
        public GameObject Object { get; set; }
    }
}
