﻿using Assets.Components.Service;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RegionService : ServiceBase
{
    private RegionScriptableObject currentRegion;

    public void EnterRegion(RegionScriptableObject region)
    {
        SceneManager.LoadScene("RegionScene");
        currentRegion = region;
    }

    public RegionScriptableObject GetCurrentRegion()
    {
        return currentRegion;
    }

    public GameObject GetBackgroundSprite()
    {
        return currentRegion.BackgroundSprite;
    }
}
