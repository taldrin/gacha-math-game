﻿using Assets.Components.Service;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapService : ServiceBase {

    public void EnterRegion(RegionScriptableObject region)
    {
        Debug.Log("Entered Region: " + region.name);
        Services.GetService<RegionService>().EnterRegion(region);
    }
}
