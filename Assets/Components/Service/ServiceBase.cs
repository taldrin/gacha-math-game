﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Components.Service
{

    public abstract class ServiceBase : MonoBehaviour
    {
        public virtual void Awake()
        {
            DontDestroyOnLoad(this);
            Services.RegisterService(this);
        }
    }
}

