﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Assets.Components.Service {

    public class InputService : ServiceBase
    {
        Dictionary<KeyCode, List<InputAction>> keyPressedListeners;
        Dictionary<KeyCode, List<InputAction>> keyHeldListeners;
        Dictionary<GameObject, InputAction> mouseRayCastListeners;
        Dictionary<GameObject, InputAction> rightMouseRayCastListeners;
        List<InputAction> inputHeldListeners;
        List<InputAction> inputPressedListeners;
        List<InputAction> inputUpListeners;

        public Vector3 lastRightClick;

        void Start()
        {

        }

        // Use this for initialization
        public override void Awake()
        {
            base.Awake();
            keyPressedListeners = new Dictionary<KeyCode, List<InputAction>>();
            keyHeldListeners = new Dictionary<KeyCode, List<InputAction>>();
            mouseRayCastListeners = new Dictionary<GameObject, InputAction>();
            rightMouseRayCastListeners = new Dictionary<GameObject, InputAction>();
            inputHeldListeners = new List<InputAction>();
            inputPressedListeners = new List<InputAction>();
            inputUpListeners = new List<InputAction>();
        }

        public void registerInputUpListener(InputAction action)
        {
            inputUpListeners.Add(action);
        }

        public void registerMouseHeldListener(InputAction action)
        {
            inputHeldListeners.Add(action);
        }

        public void registerMousePressedListener(InputAction action)
        {
            inputPressedListeners.Add(action);
        }

        public void registerHeldListener(InputAction inputAction)
        {
            try
            {
                keyHeldListeners[inputAction.Key].Add(inputAction);
            }
            catch (KeyNotFoundException e)
            {
                keyHeldListeners.Add(inputAction.Key, new List<InputAction>());
                keyHeldListeners[inputAction.Key].Add(inputAction);
            }
        }

        public void registerPressedListener(InputAction inputAction)
        {
            try
            {
                keyPressedListeners[inputAction.Key].Add(inputAction);
            }
            catch (KeyNotFoundException e)
            {
                keyPressedListeners.Add(inputAction.Key, new List<InputAction>());
                keyPressedListeners[inputAction.Key].Add(inputAction);
            }
        }

        public void registerMouseRayCastListener(InputAction inputAction)
        {
            mouseRayCastListeners.Add(inputAction.Object, inputAction);
        }

        public void unregisterMouseRayCastListener(GameObject obj)
        {
            mouseRayCastListeners.Remove(obj);
        }

        public void unregisterPressedListener(InputAction action)
        {
            keyPressedListeners[action.Key].Remove(action);
        }

        public void registerRightMouseRayCastListeners(InputAction inputAction)
        {
            rightMouseRayCastListeners.Add(inputAction.Object, inputAction);
        }

        public void unregisterRightMouseRayCastListeners(GameObject obj)
        {
            rightMouseRayCastListeners.Remove(obj);
        }


        RaycastHit2D getMouseRaycastHit()
        {
            RaycastHit2D hit;
            hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.down);
            //Physics.Raycast(rr, out hit);
            return hit;
        }

        // Update is called once per frame
        void Update()
        {
            foreach (KeyCode k in keyHeldListeners.Keys)
            {
                if (Input.GetKey(k))
                {
                    foreach (InputAction ia in keyHeldListeners[k])
                    {
                        ia.Action(ia.Return);
                    }
                }
            }
            foreach (KeyCode k in keyPressedListeners.Keys)
            {
                if (Input.GetKeyDown(k))
                {
                    List<InputAction> list = keyPressedListeners[k];
                    for (int i = 0; i < list.Count; i++)
                    {
                        var obj = list[i];
                        obj.Action(obj.Return);
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                RaycastHit2D hit = getMouseRaycastHit();
                if (hit.collider != null)
                {
                    InputAction valOut;
                    if (mouseRayCastListeners.TryGetValue(hit.collider.gameObject, out valOut))
                    {
                        valOut.Action(valOut.Return);
                    }
                } else
                {
                    foreach(var action in inputPressedListeners)
                    {
                        action.Action(Input.mousePosition);
                    }
                }
            }

            if(Input.GetKeyUp(KeyCode.Mouse0))
            {
                foreach (var action in inputUpListeners)
                {
                    action.Action(Input.mousePosition);
                }
            }

            if(Input.GetKey(KeyCode.Mouse0))
            {
                foreach (var listener in inputHeldListeners)
                {
                    listener.Action(Input.mousePosition);
                }
            }

            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                RaycastHit2D hit = getMouseRaycastHit();
                if (hit.collider == null) return;
                InputAction valOut;
                if (rightMouseRayCastListeners.TryGetValue(hit.collider.gameObject, out valOut))
                {
                    valOut.Action(valOut.Return);
                }
            }
        }
    }
}
