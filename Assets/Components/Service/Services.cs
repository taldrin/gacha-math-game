﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Components.Service
{
    public class Services
    {
        private static List<ServiceBase> services = new List<ServiceBase>();

        public static T GetService<T>() where T : ServiceBase
        {
            return (T)services.Find(a => a.GetType() == typeof(T));
        }

        public static void RegisterService(ServiceBase bs)
        {
            Debug.Log("Registering: " + bs);
            services.Add(bs);
        }
    }
}
